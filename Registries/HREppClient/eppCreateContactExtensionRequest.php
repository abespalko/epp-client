<?php
namespace Metaregistrar\EPP;
/*
 * This object contains all the logic to create an EPP create command
 */


class eppCreateContactExtensionRequest extends eppCreateContactRequest
{

	private $create;
	private $contactDom;

	function __construct($createinfo, $type = null, $in = null)
	{
		parent::__construct($createinfo);
		if ($type) {
			$this->addHRType($type);
		}
		if ($in) {
			$this->addHRIn($in);
		}
		$this->addSessionId();
	}


	public function addHRType($type)
	{
		if (!$this->extension) {
			$this->extension  = $this->createElement('extension');
			$this->create     = $this->createElement('hr:create');
			$this->contactDom = $this->createElement('hr:contact');

			$this->create->appendChild($this->contactDom);
			$this->extension->appendChild($this->create);
			$this->command->appendChild($this->extension);
		}
		$this->contactDom->appendChild($this->createElement('hr:type', $type));

	}

	public function addHRIn($in)
	{
		if (!$this->extension) {
			$this->extension  = $this->createElement('extension');
			$this->create     = $this->createElement('hr:create');
			$this->contactDom = $this->createElement('hr:contact');

			$this->create->appendChild($this->contactDom);
			$this->extension->appendChild($this->create);
			$this->command->appendChild($this->extension);
		}
		$this->contactDom->appendChild($this->createElement('hr:in', $in));
	}


}
