<?php
namespace Metaregistrar\EPP;

include_once(dirname(__FILE__) . '/eppCreateContactExtensionRequest.php');

class HREppClient extends eppHttpsConnection
{

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		parent::setHostname('registrar-test2.carnet.hr/epp/');
		parent::setUsername('Logomedia1-EPP');
		parent::setPassword('QfjHE1vxM');
		parent::setLanguage('en');
		parent::setVersion('1.0');
		parent::setServices(array(
			'urn:ietf:params:xml:ns:domain-1.0' => 'domain',
			'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
		));
	}

	/**
	 * Performs login request.
	 *
	 * @return boolean
	 */
	public function login()
	{
		try {
			$login = new eppLoginRequest;

			if ($this->writeandread($login)) {
				return true;
			}
		}
		catch (eppException $e) {

		}
		return false;
	}

	/**
	 * Returns response as array.
	 *
	 * @return mixed
	 *
	 * @throws eppException
	 */
	public function getResponseArray()
	{
		$xml = $this->read();

		$deXml = simplexml_load_string($xml);
		$deJson = json_encode($deXml);
		return json_decode($deJson, true);
	}

}