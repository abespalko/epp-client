<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	checkDomain($eppClient);
	checkDomainXML($eppClient);
}

function checkDomain(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$domainRequest = new Metaregistrar\EPP\eppCheckRequest(array('domena1.hr', 'domena2.hr'));
	$response = $eppClient->writeandread($domainRequest);
	if ($response instanceof Metaregistrar\EPP\eppCheckResponse && $response->Success()) {
		foreach ($response->getCheckedDomains() as $domain) {
			echo 'Domain: ' . $domain['domainname'] . ', Availability: ' . $domain['available'] . '<br />';
		}
	}
}

function checkDomainXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
	<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
	  <command>
		<check>
		  <domain:check>
			<domain:name>domena1.hr</domain:name>
			<domain:name>domena2.hr</domain:name>
		  </domain:check>
		</check>
		<clTRID>05106558-94309643</clTRID>
	  </command>
	</epp>
	');
	print_r($eppClient->getResponseArray());
	echo $eppClient->read();
}