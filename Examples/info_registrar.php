<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	registrarInfo($eppClient);
}

function registrarInfo(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:hr="http://www.dns.hr/epp/hr-1.0">
  <command>
    <info>
      <hr:registrar />
    </info>
    <clTRID>05106558-94309643</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");

	echo $eppClient->read();

}