<?php

require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

$eppClient->setServices(array(
	'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
));

if ($eppClient->login()) {
	contactInfo($eppClient);
//	contactInfoXML($eppClient);
}

function contactInfo(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$contact = new Metaregistrar\EPP\eppContactHandle('43762');
	$infoRequest = new Metaregistrar\EPP\eppInfoContactRequest($contact);
	if (($response = $eppClient->writeandread($infoRequest)) instanceof Metaregistrar\EPP\eppInfoContactResponse) {
		echo 'Contact ID: ' . $response->getContactId() . '<br />';
		echo 'Contact Resource ID: ' . $response->getContactRoid() . '<br />';
		echo 'Contact Client ID: ' . $response->getContactClientId() . '<br />';
		echo 'Contact Create Client ID: ' . $response->getContactCreateClientId() . '<br />';
		echo 'Contact Update Date: ' . $response->getContactUpdateDate() . '<br />';
		echo 'Contact Create Date: ' . $response->getContactCreateDate() . '<br />';
		echo 'Contact Status: ' . $response->getContactStatus() . '<br />';
		echo 'Contact Status CSV: ' . $response->getContactStatusCSV() . '<br />';
		echo 'Contact Voice: ' . $response->getContactVoice() . '<br />';
		echo 'Contact Fax: ' . $response->getContactFax() . '<br />';
		echo 'Contact Email: ' . $response->getContactEmail() . '<br />';
	}
}

function contactInfoXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:contact="urn:ietf:params:xml:ns:contact-1.0">
  <command>
    <info>
      <contact:info>
        <contact:id>131888</contact:id>
      </contact:info>
    </info>
    <clTRID>89845498-45440972</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");

	echo $eppClient->read();

}