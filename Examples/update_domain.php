<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();
if ($eppClient->login()) {
	updateDomain($eppClient);
//	updateDomainXML($eppClient);
}

function updateDomain(\Metaregistrar\EPP\HREppClient $eppClient)
{
	try {
		$domainName = 'domena11.hr';
		$registrant = 43764;
		$addHosts   = array(
			'1.domena11.hr' => array(
				'9.9.9.9',
				'1020:0:0:0:8:800:200C:417A',
			),
			'2.domena11.hr' => array(
			),
		);
		$remHosts   = array(
			'1.domena11.hr',
		);
		$contacts   = array(
			new Metaregistrar\EPP\eppContactHandle($registrant, Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_ADMIN),
			new Metaregistrar\EPP\eppContactHandle($registrant, Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_TECH),
		);

		$domain = new Metaregistrar\EPP\eppDomain($domainName);

		// Data to add.
		$add = new Metaregistrar\EPP\eppDomain($domainName);

		foreach ($addHosts as $host => $ips) {
			$h = new Metaregistrar\EPP\eppHost($host, $ips);
			$add->addHost($h);
		}

		foreach ($contacts as $contact) {
			$add->addContact($contact);
		}

		$add->addStatus(\Metaregistrar\EPP\eppDomain::STATUS_CLIENT_TRANSFER_PROHIBITED);

		$addSecdns = new Metaregistrar\EPP\eppSecdns();
		$addSecdns->setKeytag(661);
		$addSecdns->setAlgorithm(10);
		$addSecdns->setDigestType(1);
		$addSecdns->setDigest('afc97ea131fd7e2695a98ef34013608f97f34e1d');
		$add->addSecdns($addSecdns);

		// Data to remove.
		$rem = new Metaregistrar\EPP\eppDomain($domainName);

		foreach ($remHosts as $remHost) {
			$h = new Metaregistrar\EPP\eppHost($remHost);
			$rem->addHost($h);
		}

		foreach ($contacts as $contact) {
			$rem->addContact($contact);
		}

		$removeSecdns = new Metaregistrar\EPP\eppSecdns();
		$removeSecdns->setKeytag(61072);
		$removeSecdns->setAlgorithm(3);
		$removeSecdns->setDigestType(2);
		$removeSecdns->setDigest('E7DE0B5B896EB7490EC31C423D070719F0CB776B');
		$rem->addSecdns($removeSecdns);

		//Data to change.
		$chg = new Metaregistrar\EPP\eppDomain($domainName);

		$chg->setRegistrant($registrant);

		$updateRequest = new Metaregistrar\EPP\eppDnssecUpdateDomainRequest($domain, $add, $rem, $chg);

		echo $updateRequest->saveXML();
		if ((($response = $eppClient->writeandread($updateRequest)) instanceof Metaregistrar\EPP\eppUpdateResponse) && ($response->Success())) {
			echo "Domain $domainName updated, infoing\n";
		}
	} catch (Metaregistrar\EPP\eppException $e) {
		echo $e->getMessage() . "\n";
	}
}

function updateDomainXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
  <command>
    <update>
      <domain:update>
        <domain:name>domena11.hr</domain:name>
        <domain:add>
          <domain:ns>
            <domain:hostAttr>
              <domain:hostName>1.domena11.hr</domain:hostName>
              <domain:hostAddr ip="v4">9.9.9.9</domain:hostAddr>
              <domain:hostAddr ip="v6">1020:0:0:0:8:800:200C:417A</domain:hostAddr>
            </domain:hostAttr>
            <domain:hostAttr>
              <domain:hostName>2.domena11.hr</domain:hostName>
            </domain:hostAttr>
          </domain:ns>
          <domain:contact type="admin">43764</domain:contact>
          <domain:contact type="tech">43764</domain:contact>
          <domain:status s="clientTransferProhibited" lang="en"></domain:status>
        </domain:add>
        <domain:rem>
          <domain:ns>
            <domain:hostAttr>
              <domain:hostName>1.domena11.hr</domain:hostName>
            </domain:hostAttr>
          </domain:ns>
          <domain:contact type="tech">43764</domain:contact>
          <domain:contact type="admin">43764</domain:contact>
        </domain:rem>
        <domain:chg>
          <domain:registrant>43764</domain:registrant>
          <domain:authInfo>
            <domain:pw>not important</domain:pw>
          </domain:authInfo>
        </domain:chg>
      </domain:update>
    </update>
    <extension>
      <secDNS:update xmlns:secDNS="urn:ietf:params:xml:ns:secDNS-1.1">
        <secDNS:rem>
          <secDNS:dsData>
            <secDNS:keyTag>61072</secDNS:keyTag>
            <secDNS:alg>3</secDNS:alg>
            <secDNS:digestType>2</secDNS:digestType>
            <secDNS:digest>E7DE0B5B896EB7490EC31C423D070719F0CB776B</secDNS:digest>
          </secDNS:dsData>
        </secDNS:rem>
        <secDNS:add>
          <secDNS:dsData>
            <secDNS:keyTag>661</secDNS:keyTag>
            <secDNS:alg>10</secDNS:alg>
            <secDNS:digestType>1</secDNS:digestType>
            <secDNS:digest>afc97ea131fd7e2695a98ef34013608f97f34e1d</secDNS:digest>
          </secDNS:dsData>
        </secDNS:add>
      </secDNS:update>
    </extension>
    <clTRID>93540809-98256278</clTRID>
  </command>
</epp>
	');
	header("Content-type: text/xml; charset=utf-8");

	echo $eppClient->read();
}