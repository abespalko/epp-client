<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	transferDomain($eppClient);
//	transferDomainXML($eppClient);
}

function transferDomain(\Metaregistrar\EPP\HREppClient $eppClient) {
	try {
		$domainName = 'domena11.hr';

		$domain = new Metaregistrar\EPP\eppDomain($domainName);

		$transfer = new Metaregistrar\EPP\eppTransferRequest(Metaregistrar\EPP\eppTransferRequest::OPERATION_REQUEST, $domain);
//		echo $transfer->saveXML();
		if ((($response = $eppClient->writeandread($transfer)) instanceof Metaregistrar\EPP\eppTransferResponse) && ($response->Success())) {
			echo $response->saveXML();
		}

	} catch (Metaregistrar\EPP\eppException $e) {
		echo $e->getMessage() . "\n";
	}
}

function transferDomainXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
  <command>
    <transfer op="request">
      <domain:transfer>
        <domain:name>domena.hr</domain:name>
      </domain:transfer>
    </transfer>
    <clTRID>22989547-58879732</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");

	echo $eppClient->read();
}
