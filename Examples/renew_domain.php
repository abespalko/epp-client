<?php

require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
//	renewDomainXML($eppClient);
	renewDomain($eppClient);
}

function renewDomain($eppClient)
{
	try {
		$domainName = 'domena11.hr';
		$expDate = '2018-04-13';

		$domain = new Metaregistrar\EPP\eppDomain($domainName);
		$domain->setPeriodUnit('y');
		$domain->setPeriod('1');

		$renew   = new Metaregistrar\EPP\eppRenewRequest($domain, $expDate);
		if ((($response = $eppClient->writeandread($renew)) instanceof Metaregistrar\EPP\eppRenewResponse) && ($response->Success())) {
			echo "Domain $domainName renewed.";
		}

	} catch (Metaregistrar\EPP\eppException $e) {
		echo $e->getMessage() . "\n";
	}
}


function renewDomainXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
  <command>
    <renew>
      <domain:renew>
        <domain:name>domena11.hr</domain:name>
        <domain:curExpDate>2017-04-13</domain:curExpDate>
        <domain:period unit="y">1</domain:period>
      </domain:renew>
    </renew>
    <clTRID>93540809-98256278</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();
}
