<?php
header("Content-type: text/xml; charset=utf-8");

require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

$response = $eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <command>
    <login>
      <clID>Logomedia1-EPP</clID>
      <pw>QfjHE1vxM</pw>
      <options>
        <version>1.0</version>
        <lang>en</lang>
      </options>
      <svcs>
        <objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
        <objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
        <svcExtension>
          <extURI>http://www.dns.hr/epp/hr-1.0</extURI>
        </svcExtension>
      </svcs>
    </login>
    <clTRID>19874813-55215846</clTRID>
  </command>
</epp>
');

echo $eppClient->read();