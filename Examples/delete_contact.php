<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	deleteContact($eppClient);
//	deleteContactXML($eppClient);
}

function deleteContact(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->setServices(array(
		'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
	));
	$contact = new Metaregistrar\EPP\eppContactHandle('43650');
	$deleteRequest = new Metaregistrar\EPP\eppDeleteRequest($contact);
	$response = $eppClient->writeandread($deleteRequest);
	if ($response instanceof Metaregistrar\EPP\eppDeleteResponse) {
		header("Content-type: text/xml; charset=utf-8");
		echo $eppClient->read();
	}
}

function deleteContactXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:contact="urn:ietf:params:xml:ns:contact-1.0">
  <command>
    <delete>
      <contact:delete>
        <contact:id>43624</contact:id>
      </contact:delete>
    </delete>
    <clTRID>89845498-44440972</clTRID>
  </command>
</epp>
');

	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();
}