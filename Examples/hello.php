<?php
header("Content-type: text/xml; charset=utf-8");

require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	$response = $eppClient->write('
<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <hello />
</epp>
');

	echo $eppClient->read();
}
