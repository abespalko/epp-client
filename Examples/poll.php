<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	poll($eppClient);
//	pollXML($eppClient);
}

function poll(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$pollRequest = new Metaregistrar\EPP\eppPollRequest(Metaregistrar\EPP\eppPollRequest::POLL_REQ);
	if (($response = $eppClient->writeandread($pollRequest)) instanceof Metaregistrar\EPP\eppPollResponse && $response->Success()) {
//		header("Content-type: text/xml; charset=utf-8");
//		echo 'Domain name = ' . $response->getDomainName();
	}
}

function pollXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
  <command>
    <poll op="req" />
    <clTRID>ABC-12345</clTRID>
  </command>
</epp>
');

	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();
}