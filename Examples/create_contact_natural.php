<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	createContactNatural($eppClient);
//	createContactNaturalXML($eppClient);
}

function createContactNatural(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->setServices(array(
		'http://www.dns.hr/epp/hr-1.0'       => 'hr',
		'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
	));
	$postalInfo = new Metaregistrar\EPP\eppContactPostalInfo('Ime Prezime', 'Grad', 'HR', 'Grad', 'Ulica', null, '10000', Metaregistrar\EPP\eppContactPostalInfo::POSTAL_TYPE_INTERNATIONAL);
	$contactInfo = new Metaregistrar\EPP\eppContact($postalInfo, 'email@email.hr', '+385.156845', '+385.156845');
	$contact = new Metaregistrar\EPP\eppCreateContactExtensionRequest($contactInfo, 'person', '12345');
	if ((($response = $eppClient->writeandread($contact)) instanceof Metaregistrar\EPP\eppCreateResponse) && $response->Success()) {
//		header("Content-type: text/xml; charset=utf-8");
//		echo $eppClient->read();
		echo 'New contact id: ' . $response->getContactId() . '<br />';
		echo 'Created date: ' . $response->getContactCreateDate() . '<br />';
	}
}

function createContactNaturalXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:contact="urn:ietf:params:xml:ns:contact-1.0" xmlns:hr="http://www.dns.hr/epp/hr-1.0">
  <command>
    <create>
      <contact:create>
        <contact:id>123</contact:id>
        <contact:postalInfo type="int">
          <contact:name>Ime Prezime</contact:name>
          <contact:addr>
            <contact:street>Ulica</contact:street>
            <contact:city>Grad</contact:city>
            <contact:pc>10000</contact:pc>
            <contact:cc>HR</contact:cc>
          </contact:addr>
        </contact:postalInfo>
        <contact:voice>+385.156845</contact:voice>
        <contact:fax>+385.156845</contact:fax>
        <contact:email>email@email.hr</contact:email>
        <contact:authInfo>
          <contact:pw>not important</contact:pw>
        </contact:authInfo>
      </contact:create>
    </create>
    <extension>
      <hr:create>
        <hr:contact>
          <hr:type>person</hr:type>
          <hr:in>10162055275</hr:in>
        </hr:contact>
      </hr:create>
    </extension>
    <clTRID>60084428-60317586</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();
}
