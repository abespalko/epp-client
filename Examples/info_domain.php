<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	domainInfo($eppClient);
//	domainInfoXML($eppClient);
}


function domainInfo(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$domainName = 'domena11.hr';
	$domain = new Metaregistrar\EPP\eppDomain($domainName);
	$infoResponse = new Metaregistrar\EPP\eppInfoDomainRequest($domain);
	if (($response = $eppClient->writeandread($infoResponse)) instanceof Metaregistrar\EPP\eppInfoDomainResponse) {
		header("Content-type: text/xml; charset=utf-8");
		echo $eppClient->read();
	}
}

function domainInfoXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
  <command>
    <info>
      <domain:info>
        <domain:name>domena1.hr</domain:name>
      </domain:info>
    </info>
    <clTRID>05106558-94309643</clTRID>
  </command>
</epp>
');

	echo $eppClient->read();
}