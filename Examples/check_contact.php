<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();
$eppClient->setServices(array(
	'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
));

if ($eppClient->login()) {

	checkContactXML($eppClient);
	checkContact($eppClient);
}


function checkContact(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$contacts = array(
		'43649',
		'43753'
	);
	$checkRequest = new Metaregistrar\EPP\eppCheckRequest();
	$checkRequest->setContactHandles($contacts);

	$response = $eppClient->writeandread($checkRequest);
	if ($response instanceof Metaregistrar\EPP\eppCheckResponse && $response->Success()) {
//		header("Content-type: text/xml; charset=utf-8");
//		echo $eppClient->read();
		var_dump($response->getCheckedContacts());
		foreach ($response->getCheckedContacts() as $contactId => $avail) {
			echo 'Checked contact id: ' . $contactId . ', Availability: ' . $avail . '<br />';
		}
	}

}

function checkContactXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
  <command>
    <check>
      <contact:check xmlns:contact="urn:ietf:params:xml:ns:contact-1.0">
        <contact:id>12345</contact:id>
        <contact:id>56789</contact:id>
      </contact:check>
    </check>
    <clTRID>ABC-12345</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();

}