<?php
require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();
$eppClient->setServices(array(
	'urn:ietf:params:xml:ns:contact-1.0' => 'contact'
));

if ($eppClient->login()) {
	updatecontact($eppClient);
	updatecontactXML($eppClient);
}


function updateContact(\Metaregistrar\EPP\HREppClient $eppClient) {

	$contactId = '43649';

	$contact = new Metaregistrar\EPP\eppContactHandle($contactId);
	$update = new Metaregistrar\EPP\eppContact();
	$update->setEmail('test@test.hr');
	$up = new Metaregistrar\EPP\eppUpdateContactRequest($contact, null, null, $update);
	if (($response = $eppClient->writeandread($up)) instanceof Metaregistrar\EPP\eppUpdateResponse) {
		header("Content-type: text/xml; charset=utf-8");
		echo $eppClient->read();
	}
}

function updateContactXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:contact="urn:ietf:params:xml:ns:contact-1.0" xmlns:hr="http://www.dns.hr/epp/hr-1.0">
  <command>
    <update>
      <contact:update>
        <contact:id>43649</contact:id>
        <contact:chg>
          <contact:postalInfo type="int">
            <contact:name>New Name</contact:name>
            <contact:addr>
              <contact:street>New street</contact:street>
              <contact:city>New city</contact:city>
              <contact:pc>New pc</contact:pc>
              <contact:cc>DE</contact:cc>
            </contact:addr>
          </contact:postalInfo>
          <contact:voice>+385.88888</contact:voice>
          <contact:fax>+385.77777</contact:fax>
          <contact:email>newemail@newemail.com</contact:email>
        </contact:chg>
      </contact:update>
    </update>
    <extension>
      <hr:update>
        <hr:contact>
          <hr:type>org</hr:type>
          <hr:in>12345678910</hr:in>
        </hr:contact>
      </hr:update>
    </extension>
    <clTRID>60084428-60317587</clTRID>
  </command>
</epp>
');
	header("Content-type: text/xml; charset=utf-8");

	echo $eppClient->read();
}
