<?php

require('../autoloader.php');

$eppClient = new \Metaregistrar\EPP\HREppClient();

if ($eppClient->login()) {
	createDomain($eppClient);
//	createDomainXML($eppClient);
}

function createDomain(\Metaregistrar\EPP\HREppClient $eppClient)
{
	try {
		$domainName        = 'domena1172.com.hr';
		$registrant        = 43779;
		$admin			= 43780;
		$tech			= 43781;
		$contacts          = array(
			new Metaregistrar\EPP\eppContactHandle($admin, Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_ADMIN),
			new Metaregistrar\EPP\eppContactHandle($tech, Metaregistrar\EPP\eppContactHandle::CONTACT_TYPE_TECH),
		);
		$hosts             = null;
		$period            = 1;
		$authorisationCode = null;
		/**
		$nameservers = array(
		'ns1.logoims.com' => array(
		'1.2.3.4',
		'1080:0:0:0:8:800:200C:417A',
		),
		'ns2.logoims.com' => array(
		'1.2.3.4',
		'1080:0:0:0:8:800:200C:417A',
		)
		);	
		 **/
		$nameservers = array(
			'1.domena117.com.hr' => array(
				'9.9.9.9',
				'1020:0:0:0:8:800:200C:417A',
			),
			'ns1.logoims.com' => array(
			)
		);

		$domain = new Metaregistrar\EPP\eppDomain($domainName, $registrant, $contacts, $hosts, $period, $authorisationCode);

		$domain->setRegistrant($registrant);

		$domain->setAuthorisationCode($domain->generateRandomString(12));
		/**
		$secDns = new Metaregistrar\EPP\eppSecdns();
		$secDns->setSiglife(604800);
		$secDns->setKeytag(551);
		$secDns->setAlgorithm(5);
		$secDns->setDigestType(1);
		$secDns->setDigest('f10e2821bbbea527ea02200352313bc059445190');

		$domain->addSecdns($secDns);

		$secDns->setKeytag(536);
		$secDns->setAlgorithm(3);
		$secDns->setDigestType(2);
		$secDns->setDigest('eaa67f3a93d0acb08d8a5e8ff9866f51983b3c3b');

		$domain->addSecdns($secDns);

		 **/
		$create = new Metaregistrar\EPP\eppCreateDomainRequest($domain);

		foreach ($nameservers as $nameserver => $ips) {
			$host = new Metaregistrar\EPP\eppHost($nameserver, $ips);
			$domain->addHost($host);
		}



//		echo $create->saveXML();
		if ((($response = $eppClient->writeandread($create)) instanceof Metaregistrar\EPP\eppCreateResponse) && $response->Success()) {
			echo "Domain " . $response->getDomainName() . " created on " . $response->getDomainCreateDate() . ", expiration date is " . $response->getDomainExpirationDate() . "\n";
		}
	} catch (Metaregistrar\EPP\eppException $e) {
		echo $e->getMessage() . "\n";
	}
}

function createDomainXML(\Metaregistrar\EPP\HREppClient $eppClient)
{
	$eppClient->write('<?xml version="1.0" encoding="utf-8"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
  <command>
    <create>
      <domain:create>
        <domain:name>domena223.hr</domain:name>
        <domain:period unit="y">1</domain:period>
        <domain:ns>
          <domain:hostAttr>
            <domain:hostName>1.domena223.hr</domain:hostName>
            <domain:hostAddr ip="v4">1.2.3.4</domain:hostAddr>
            <domain:hostAddr ip="v6">1080:0:0:0:8:800:200C:417A</domain:hostAddr>
          </domain:hostAttr>
          <domain:hostAttr>
            <domain:hostName>2.domena223.hr</domain:hostName>
          </domain:hostAttr>
        </domain:ns>
        <domain:registrant>43764</domain:registrant>
        <domain:contact type="admin">43760</domain:contact>
        <domain:contact type="tech">43760</domain:contact>
        <domain:authInfo>
          <domain:pw>123</domain:pw>
        </domain:authInfo>
      </domain:create>
    </create>
    <extension>
      <secDNS:create xmlns:secDNS="urn:ietf:params:xml:ns:secDNS-1.1">
        <secDNS:maxSigLife>604800</secDNS:maxSigLife>
        <secDNS:dsData>
          <secDNS:keyTag>551</secDNS:keyTag>
          <secDNS:alg>5</secDNS:alg>
          <secDNS:digestType>1</secDNS:digestType>
          <secDNS:digest>f10e2821bbbea527ea02200352313bc059445190</secDNS:digest>
        </secDNS:dsData>
        <secDNS:dsData>
          <secDNS:keyTag>536</secDNS:keyTag>
          <secDNS:alg>3</secDNS:alg>
          <secDNS:digestType>2</secDNS:digestType>
          <secDNS:digest>eaa67f3a93d0acb08d8a5e8ff9866f51983b3c3b</secDNS:digest>
        </secDNS:dsData>
      </secDNS:create>
    </extension>
    <clTRID>93540809-98256278</clTRID>
  </command>
</epp>
	');
	header("Content-type: text/xml; charset=utf-8");
	echo $eppClient->read();
}